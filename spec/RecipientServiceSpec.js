var RecipientServiceImport = require('./../src/main/resources/static/gift/RecipientService.js');
var AbstractServiceImport = require('./../src/main/resources/static/gift/AbstractService.js');

var AbstractService = AbstractServiceImport.AbstractService;
var RecipientService = RecipientServiceImport.RecipientService;

var Recipient = function() {};

function mockRequestFactory(response, status) {
    return {
        create: function () {
            return {
                readyState: 0,
                status: 0,
                response: "",
                open: function () {
                },
                setRequestHeader: function (type, value) {
                },
                send: function () {
                    this.readyState = 4;
                    this.status = status;
                    this.response = response;
                    this.onreadystatechange()
                }
            }
        }
    };
}

var identityParser = {
    destringify: x => x,
    stringify: x => x
}

describe("RecipientService", function() {
    describe("getRecipients", function() {
        it("calls the 'onSuccess' callback if the request was successful", function() {

            var requesetFactory = mockRequestFactory([new Recipient()], 200);
            var service = new RecipientService(new AbstractService(requesetFactory, identityParser));
            var callbacksMade = 0;
            var onSuccess = function(recipients) {
                callbacksMade++;
            }
            var onFailure = function() {
                callbacksMade++;
                fail("Should not call 'onFailure'");
            }
            service.getAll(onSuccess, null);
            expect(callbacksMade).toBe(1);
        });
        it("calls the 'onFailure' callback if the specified recipient is not found", function() {
            var requesetFactory = mockRequestFactory([], 404);
            var service = new RecipientService(new AbstractService(requesetFactory, identityParser));
            var callbacksMade = 0;
            var onSuccess = function() {
                callbacksMade++;
                fail("Should not call 'onSuccess'");
            }
            var onFailure = function(status) {
                expect(status).toBe(404);
                callbacksMade++;
            }
            service.getAll(onSuccess, onFailure);
            expect(callbacksMade).toBe(1);
        });
    });
});