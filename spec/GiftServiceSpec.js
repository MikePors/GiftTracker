var testModels = require('./TestModels');
var abstractServiceImport = require('./../src/main/resources/static/gift/AbstractService');
var giftServiceImport = require('./../src/main/resources/static/gift/GiftService');

var Gift = testModels.Gift;
var Money = testModels.Money;
var Recipient = testModels.Recipient;
var AbstractService = abstractServiceImport.AbstractService;
var GiftService = giftServiceImport.GiftService;

var idGiftParser = testModels.idGiftParser;

function mockRequestFactory(response, status) {
    return {
        create: function () {
            return {
                readyState: 0,
                status: 0,
                response: "",
                open: function () {
                },
                setRequestHeader: function (type, value) {
                },
                send: function () {
                    this.readyState = 4;
                    this.status = status;
                    this.response = response;
                    this.onreadystatechange()
                }
            }
        }
    };
}

describe("GiftService", function () {
    describe ("getAll", function () {
        it("provides empty when the request is successful but there are no gifts", function () {
            var requestFactory = mockRequestFactory([], 200);
            var giftService = new GiftService(new AbstractService(requestFactory, idGiftParser));
            var callbackCallCount = 0;
            var onSuccess = function (allGifts) {
                expect(allGifts).toEqual([]);
                callbackCallCount++;
            };
            var onFailure = function () {
                fail("Something went wrong.");
                callbackCallCount++;
            };
            giftService.getAll(onSuccess, onFailure);
            expect(callbackCallCount).toBe(1);
        });
        it ("calls the onFailure callback when the request fails", function () {
            var requestFactory = mockRequestFactory("There was a failure", 404);
            var giftService = new GiftService(new AbstractService(requestFactory, idGiftParser));
            var callbackCallCount = 0;
            var onSuccess = function (allGifts) {
                callbackCallCount++;
                fail("request was successful");
            };
            var onFailure = function (errorCode) {
                callbackCallCount++;
                expect(errorCode).toBe(404);
            };
            giftService.getAll(onSuccess, onFailure);
            expect(callbackCallCount).toBe(1);
        });
        it("provides all gifts when the request is successful (and gifts have been saved)", function () {
            var gifts = [
                new Gift("bicycle", new Money(300, 'CAD'), new Recipient(1, "Jonny"), true),
                new Gift("iTunes Gift Card", new Money(50, "USD"), new Recipient(2, "Mom"), false)
            ];
            var requestFactory = mockRequestFactory(gifts, 200);
            var giftService = new GiftService(new AbstractService(requestFactory, idGiftParser));
            var callbackCallCount = 0;
            var onSuccess = function (allGifts) {
                callbackCallCount++;
                expect(allGifts).toBe(gifts);
            };
            var onFailure = function (errorCode) {
                callbackCallCount++;
                fail("onFailure called when should have been success");
            };
            giftService.getAll(onSuccess, onFailure);
            expect(callbackCallCount).toBe(1);
        })
    });
    describe("save", function () {
        it("accepts a gift and sends a save request to the server", function () {
            var request = {
                setRequestHeader: function (key, value) {
                    if (key === 'Content-type') {
                        this.contentType = value;
                    }
                },
                open: function (method, path) {
                    this.method = method;
                    this.path = path;
                },
                send: function (body) {
                    this.body = body;
                }
            };
            var requestFactory = {
                create: function () {
                    return request;
                }
            };
            var giftService = new GiftService(new AbstractService(requestFactory, idGiftParser));
            var aGift = new Gift("Some Gift", new Money(3, 'GBP'), new Recipient(44, "Sue"), false);
            giftService.save(aGift);
            expect(request.body).toEqual(aGift);
            expect(request.contentType).toBe('application/json');
            expect(request.method).toBe('POST');
            expect(request.path).toBe('/gift/create');
        })
    })
});