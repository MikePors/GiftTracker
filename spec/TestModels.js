var Gift = function (name, value, recipient, isGifted) {
    this.name = name;
    this.value = value;
    this.recipient = recipient;
    this.isGifted = isGifted;
};

var Money = function (value, currency) {
    this.value = value;
    this.currency = currency;
};

var Recipient = function (id, name) {
    this.id = id;
    this.name = name;
};

var identity = function (x) {
    return x;
};

var idGiftParser = {
    stringify: identity,
    destringify: identity
};

exports.Gift = Gift;
exports.Money = Money;
exports.Recipient = Recipient;
exports.idGiftParser = idGiftParser;