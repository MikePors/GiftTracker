var testModelsImport = require('./TestModels');
var giftControllerImport = require("./../src/main/resources/static/gift/GiftController.js");

var Gift = testModelsImport.Gift;
var Money = testModelsImport.Money;
var Recipient = testModelsImport.Recipient;
var idGiftParser = testModelsImport.idGiftParser;

var MockGiftTable = function () {
    this.gifts = [];
    this.statusCode = 0;
    this.createRow = function (gift) {
        this.gifts.push(gift);
    };
    this.clear = function () {
        this.gifts = [];
    };
    this.displayError = function(statusCode) {
        this.statusCode = statusCode;
    }
};

var MockSuccessfulGiftService = function (giftsToReturn) {
    this.giftsToReturn = giftsToReturn;
    this.getAll = function(onSuccess, onFailure) {
        onSuccess(giftsToReturn);
    }
};

var MockFailingGiftService = function (statusCode) {
    this.statusCode = statusCode;
    this.getAll = function (onSuccess, onFailure) {
        onFailure(this.statusCode);
    }
};

describe("GiftController", function () {
    describe("getAllGifts", function () {
        it("presents an empty table when there are no gifts", function () {
            var giftTable = new MockGiftTable();
            var mockGiftService = new MockSuccessfulGiftService([]);
            var giftController =  new giftControllerImport.GiftController(giftTable, mockGiftService);
            giftController.getAllGifts();
            expect(giftTable.gifts).toEqual([]);
        });
        it("gets and displays all gifts", function () {
            var allGifts = [new Gift("Ball", new Money(1, 'CAD'), new Recipient(3, "Random Kid"), true)];
            var giftTable = new MockGiftTable();
            var mockGiftService = new MockSuccessfulGiftService(allGifts);

            var giftController = new giftControllerImport.GiftController(giftTable, mockGiftService);
            giftController.getAllGifts();
            expect(giftTable.gifts).toEqual(allGifts);
        });
        it("it clears the table before re-populating it", function () {
            var allGifts = [new Gift("Ball", new Money(1, 'CAD'), new Recipient(3, "Random Kid"), true)];
            var giftTable = new MockGiftTable();
            var mockGiftService = new MockSuccessfulGiftService(allGifts);

            var giftController = new giftControllerImport.GiftController(giftTable, mockGiftService);
            giftController.getAllGifts();
            giftController.getAllGifts();
            expect(giftTable.gifts).toEqual(allGifts);
        });
        it("does not clear the table in the event of a failure", function () {
            var allGifts = [new Gift("Ball", new Money(1, 'CAD'), new Recipient(3, "Random Kid"), true)];
            var giftTable = new MockGiftTable();
            giftTable.gifts = allGifts;
            var mockGiftService = new MockFailingGiftService(404);

            var giftController = new giftControllerImport.GiftController(giftTable, mockGiftService);
            giftController.getAllGifts();
            expect(giftTable.gifts).toEqual(allGifts);
            expect(giftTable.statusCode).toBe(404);
        })
    });
    describe("saveGift", function () {
        it("retrieves a gift from the gift form and tries to  save it", function () {
            var aGift = new Gift("Some gift", new Money(1, 'CAD'), new Recipient(100, 'Grandma'), false);
            var saveCallCount = 0;
            var mockService = {
                giftParser: idGiftParser,
                savedGifts: [],
                save: function(gift) {
                    this.savedGifts.push(gift);
                }
            };
            var giftForm = {
                get: function () {
                    return aGift;
                }
            };
            var giftController = new giftControllerImport.GiftController(null, mockService, giftForm);
            giftController.save();
            expect(mockService.savedGifts).toEqual([aGift]);
        })
    })
});
