package com.mikepors.app.controllers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mikepors.app.Application;
import com.mikepors.gifttracker.model.Recipient;
import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyedEntity;
import com.mikepors.gifttracker.model.access.Repository;
import com.mikepors.gifttracker.model.access.impl.SimpleKeyFactory;
import com.mikepors.gifttracker.model.access.impl.SimpleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecipientControllerTest {

	@LocalServerPort
	private int _port;

	@Autowired
	private TestRestTemplate _template;

	@Test
	public void createRecipient_nameProvided_keyReturned() throws URISyntaxException, MalformedURLException {
		ResponseEntity<? extends Key<Recipient>> response = doCreateRecipientRequest("Foo");

		Key<Recipient> key = response.getBody();
		Assert.assertEquals(new SimpleKeyFactory().create(0), key);
	}

	@Test
	public void createRecipient_twoRecipientsCreated_differentKeysReturned() throws MalformedURLException, URISyntaxException {
		ResponseEntity<TestRecipientKey> responseEntity = doCreateRecipientRequest("Foo");
		TestRecipientKey firstKey = responseEntity.getBody();

		responseEntity = doCreateRecipientRequest("Bar");
		TestRecipientKey secondKey = responseEntity.getBody();

		Assert.assertNotEquals(firstKey.getId(), secondKey.getId());
	}

	@Test
	public void getRecipient_singleRecipientCreated_getWithProvidedKeyReturnsOriginalRecipient() throws MalformedURLException, URISyntaxException {
		ResponseEntity<TestRecipientKey> responseEntity = doCreateRecipientRequest("Foo");
		TestRecipientKey key = responseEntity.getBody();

		Recipient expected = new Recipient("Foo");
		Recipient actual = _template.getForEntity(getRecipientRequest(key), Recipient.class).getBody();

		Assert.assertEquals(expected, actual);
	}

	private URI getRecipientRequest(TestRecipientKey key) throws URISyntaxException, MalformedURLException {
		return new URL("http://localhost:" +  _port + "/recipient/" + key.getId()).toURI();
	}

	private ResponseEntity<TestRecipientKey> doCreateRecipientRequest(String recipientName) throws MalformedURLException, URISyntaxException {
		return _template.postForEntity(createRecipientRequest(recipientName),
									   new TestRequest(recipientName),
									   TestRecipientKey.class);
	}


	private URI createRecipientRequest(@SuppressWarnings("SameParameterValue") String recipientName) throws MalformedURLException, URISyntaxException {
		return new URL("http://localhost:" + _port + "/recipient/create/").toURI();
	}

	private static class TestRecipientKey implements Key<Recipient> {

		private long _id;

		@JsonCreator
		TestRecipientKey(@JsonProperty(value = "id", required = true) Long id) {
			_id = id;
		}


		@Override
		public long getId() {
			return _id;
		}
	}

	public static class TestRequest {
		String _name;

		TestRequest(String name) {
			_name = name;
		}

		@SuppressWarnings("unused")
		public String getName() {
			return _name;
		}
	}

	@TestConfiguration
	public static class TestContext extends Application {

		@Override
		@Bean
		public Repository<Recipient> getRecipientRepository() {
			return new SimpleRepository<>(new SimpleKeyFactory<>());
		}
	}
}
