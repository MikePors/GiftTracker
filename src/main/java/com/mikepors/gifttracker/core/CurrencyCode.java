package com.mikepors.gifttracker.core;

public enum CurrencyCode {
	CAD,
	USD
}
