package com.mikepors.gifttracker.core;

import java.io.Serializable;
import java.util.Objects;

public final class Money {

	private final long _valueInMillionths;

	private final CurrencyCode _currencyCode;

	public static Money of(double value, CurrencyCode currencyCode) {
		long valueInMillionths = Math.round(value * 1_000_000);
		return new Money(valueInMillionths, currencyCode);
	}

	public static Money ofMillionths(long valueInMillionths, CurrencyCode currencyCode) {
		return new Money(valueInMillionths, currencyCode);
	}

	private Money(long valueInMillionths,
				 CurrencyCode currencyCode) {
		_valueInMillionths = valueInMillionths;
		_currencyCode = currencyCode;
	}

	public long getValueInMillionths() {
		return _valueInMillionths;
	}

	public CurrencyCode getCurrencyCode() {
		return _currencyCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Money money = (Money) o;
		return _valueInMillionths == money._valueInMillionths &&
				_currencyCode == money._currencyCode;
	}

	@Override
	public int hashCode() {
		return Objects.hash(_valueInMillionths, _currencyCode);
	}

	@Override
	public String toString() {
		return "Money{" +
				"_valueInMillionths=" + _valueInMillionths +
				", _currencyCode=" + _currencyCode +
				'}';
	}
}
