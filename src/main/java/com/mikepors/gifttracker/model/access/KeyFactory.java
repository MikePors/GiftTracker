package com.mikepors.gifttracker.model.access;

public interface KeyFactory<T> {
	Key<T> create(long id);
}
