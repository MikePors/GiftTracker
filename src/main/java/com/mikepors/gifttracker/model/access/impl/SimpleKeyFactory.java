package com.mikepors.gifttracker.model.access.impl;

import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyFactory;

public class SimpleKeyFactory<T> implements KeyFactory<T> {
	@Override
	public Key<T> create(long id) {
		return new SimpleKey<>(id);
	}
}
