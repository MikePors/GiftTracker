package com.mikepors.gifttracker.model.access.impl;

import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyedEntity;

public class SimpleKeyedEntity<T> implements KeyedEntity<T> {

	private final Key<T> _key;
	private final T _entity;

	public SimpleKeyedEntity(Key<T> key, T entity) {
		_key = key;
		_entity = entity;
	}

	@Override
	public Key<T> getKey() {
		return _key;
	}

	@Override
	public T getEntity() {
		return _entity;
	}
}
