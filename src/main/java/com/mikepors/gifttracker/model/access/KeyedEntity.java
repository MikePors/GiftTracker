package com.mikepors.gifttracker.model.access;

public interface KeyedEntity<T> {

	Key<T> getKey();

	T getEntity();
}
