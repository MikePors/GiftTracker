package com.mikepors.gifttracker.model.access.impl;

import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyFactory;
import com.mikepors.gifttracker.model.access.KeyedEntity;
import com.mikepors.gifttracker.model.access.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SimpleRepository<T> implements Repository<T> {
	private final Map<Key<T>, T> _backingMap = new HashMap<>();
	private final KeyFactory<T> _keyFactory;

	private long _nextId = 0;

	public SimpleRepository(KeyFactory<T> keyFactory) {
		_keyFactory = keyFactory;
	}

	@Override
	public Key<T> save(T t) {
		Key<T> key = _keyFactory.create(_nextId++);
		_backingMap.put(key, t);
		return key;
	}

	@Override
	public T get(Key<T> key) {
		return _backingMap.get(key);
	}

	@Override
	public List<KeyedEntity<T>> getAll() {
		return _backingMap.entrySet()
				.stream()
				.map(entry -> new SimpleKeyedEntity<>(entry.getKey(), entry.getValue()))
				.collect(Collectors.toList());
	}
}
