package com.mikepors.gifttracker.model.access;

import java.util.List;

public interface Repository<T> {
	Key<T> save(T t);
	T get(Key<T> key);

	List<KeyedEntity<T>> getAll();
}
