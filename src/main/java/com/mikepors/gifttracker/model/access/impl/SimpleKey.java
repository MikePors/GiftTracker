package com.mikepors.gifttracker.model.access.impl;

import com.mikepors.gifttracker.model.access.Key;

import java.util.Objects;

public final class SimpleKey<T> implements Key<T> {
	private final long _id;

	public SimpleKey(long id) {
		_id = id;
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Key)) return false;
		Key<?> otherKey = (Key<?>) o;
		return _id == otherKey.getId();
	}

	@Override
	public int hashCode() {
		return Objects.hash(_id);
	}
}
