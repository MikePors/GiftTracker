package com.mikepors.gifttracker.model.access;

public interface Key<T> {
	long getId();
}
