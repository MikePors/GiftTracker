package com.mikepors.gifttracker.model;

import com.mikepors.gifttracker.core.Money;
import com.mikepors.gifttracker.model.access.KeyedEntity;

public class Gift {

	private final String _name;
	private final Money _value;
	private final KeyedEntity<Recipient> _recipient;
	private final boolean _gifted;

	public Gift(String name, Money value, KeyedEntity<Recipient> recipient, boolean gifted) {
		_name = name;
		_value = value;
		_recipient = recipient;
		_gifted = gifted;
	}

	public String getName() {
		return _name;
	}

	public Money getValue() {
		return _value;
	}

	public KeyedEntity<Recipient> getRecipient() {
		return _recipient;
	}

	public boolean isGifted() {
		return _gifted;
	}

	@Override
	public String toString() {
		return "Gift{" +
				"_name='" + _name + '\'' +
				", _value=" + _value +
				", _recipient=" + _recipient +
				", _gifted=" + _gifted +
				'}';
	}
}