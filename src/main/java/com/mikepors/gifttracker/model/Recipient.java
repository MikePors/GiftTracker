package com.mikepors.gifttracker.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public final class Recipient {

	private final String _name;

	@JsonCreator
	public Recipient(@JsonProperty(value = "name") String name) {
		_name = name;
	}

	@SuppressWarnings("WeakerAccess")
	public String getName() {
		return _name;
	}

	@Override
	public int hashCode() {

		return Objects.hash(getName());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Recipient)) return false;
		Recipient recipient = (Recipient) o;
		return Objects.equals(getName(), recipient.getName());
	}
}
