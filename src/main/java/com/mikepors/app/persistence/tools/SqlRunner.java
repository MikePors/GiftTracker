package com.mikepors.app.persistence.tools;

import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyFactory;
import com.mikepors.gifttracker.model.access.impl.SimpleKey;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqlRunner {
	private StringBuilder _sqlStatement = new StringBuilder();
	private List<SqlParameter> _parameters = new ArrayList<>();

	private final DataSource _dataSource;

	public SqlRunner(DataSource dataSource) {
		_dataSource = dataSource;
	}


	public SqlRunner sql(String sql) {
		_sqlStatement.append(sql);
		return this;
	}

	public SqlRunner parameter(String parameter) {
		_parameters.add((preparedStatement, parameterIndex) -> preparedStatement.setString(parameterIndex, parameter));
		return this;
	}

	public SqlRunner parameter(long parameter) {
		_parameters.add((preparedStatement, parameterIndex) -> preparedStatement.setLong(parameterIndex, parameter));
		return this;
	}

	public SqlRunner parameter(Key<?> key) {
		_parameters.add((preparedStatement, parameterIndex) -> preparedStatement.setLong(parameterIndex, key.getId()));
		return this;
	}

	public SqlRunner parameter(boolean parameter) {
		_parameters.add((preparedStatement, parameterIndex) -> preparedStatement.setBoolean(parameterIndex, parameter));
		return this;
	}

	public <T> Key<T> insertSingleRow() {
		Key<T> key;
		try (Connection connection = _dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(_sqlStatement.toString(),
																			   Statement.RETURN_GENERATED_KEYS)) {

			for (int i = 0; i < _parameters.size(); i++) {
				SqlParameter sqlParameter = _parameters.get(i);
				sqlParameter.update(preparedStatement, i+1);
			}

			int addedRows = preparedStatement.executeUpdate();
			sanityCheck(connection, addedRows);

			key = extractKey(connection, preparedStatement);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return key;
	}

	public <T> T getSingleEntity(RowInflater<T> rowInflater) {
		T ret;
		List<T> keyedEntities = get(rowInflater);
		if (keyedEntities.size() > 1) {
			T value = keyedEntities.get(0);
			throw new IllegalStateException("Found multiple entities with the same id. [val=" + value + "]");
		} else if (keyedEntities.isEmpty()) {
			ret = null;
		} else {
			ret = keyedEntities.get(0);
		}
		return ret;
	}

	public <T> List<T> get(RowInflater<T> rowInflater) {
		List<T> values = new ArrayList<>();
		try (Connection connection = _dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(_sqlStatement.toString())) {

			for (int i = 0; i < _parameters.size(); i++) {
				SqlParameter sqlParameter = _parameters.get(i);
				sqlParameter.update(preparedStatement, i+1);
			}

			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				T t = rowInflater.inflate(resultSet);
				values.add(t);
			}

			resultSet.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return values;
	}

	private <T> Key<T> extractKey(Connection connection, PreparedStatement preparedStatement) throws SQLException {
		Key<T> key;
		ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys();
		List<Long> generatedKeys = new ArrayList<>();

		while (generatedKeysResult.next()) {
			generatedKeys.add(generatedKeysResult.getLong(1));
		}

		generatedKeysResult.close();

		sanityCheck(connection, generatedKeys.size());
		key = new SimpleKey<>(generatedKeys.get(0));
		return key;
	}

	private void sanityCheck(Connection connection, int addedRows) throws SQLException {
		if (addedRows > 1) {
			connection.rollback();
			throw new IllegalStateException("Added more rows than anticipated.  Expected 1. Added: " + addedRows);
		} else if (addedRows < 1) {
			connection.rollback();
			throw new IllegalStateException("Added fewer rows than anticipated.  Expected: 1. Added: " + addedRows);
		}
	}

	public interface RowInflater<T> {
		T inflate(ResultSet resultSet) throws SQLException;
	}

	private interface SqlParameter {
		void update(PreparedStatement preparedStatement, int parameterIndex) throws SQLException;
	}
}
