package com.mikepors.app.persistence;

import com.mikepors.app.persistence.tools.SqlRunner;
import com.mikepors.gifttracker.model.Recipient;
import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyedEntity;
import com.mikepors.gifttracker.model.access.Repository;
import com.mikepors.gifttracker.model.access.impl.SimpleKey;
import com.mikepors.gifttracker.model.access.impl.SimpleKeyFactory;
import com.mikepors.gifttracker.model.access.impl.SimpleKeyedEntity;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class SqlRecipientRepository implements Repository<Recipient> {

	@Autowired
	private DataSource _dataSource;

	@Override
	public Key<Recipient> save(Recipient recipient) {
		return new SqlRunner(_dataSource)
				.sql("INSERT INTO recipient (name)\n")
				.sql("VALUES (?)").parameter(recipient.getName())
				.insertSingleRow();
	}

	@Override
	public Recipient get(Key<Recipient> key) {
		return new SqlRunner(_dataSource)
				.sql("SELECT recipient_id, name\n")
				.sql("FROM recipient\n")
				.sql("WHERE recipient_id = ?").parameter(key)
				.getSingleEntity(new RecipientInflater()::inflateEntity)
				.getEntity();
	}

	@Override
	public List<KeyedEntity<Recipient>> getAll() {
		return new SqlRunner(_dataSource)
				.sql("SELECT recipient_id, name\n")
				.sql("FROM recipient\n")
				.get(new RecipientInflater()::inflateEntity);
	}

	private static class RecipientInflater {
		private KeyedEntity<Recipient> inflateEntity(ResultSet resultSet) throws SQLException {
			long recipientId = resultSet.getLong("recipient_id");
			SimpleKey<Recipient> key = new SimpleKey<>(recipientId);
			String name = resultSet.getString(2);
			return new SimpleKeyedEntity<>(key, new Recipient(name));
		}

	}
}
