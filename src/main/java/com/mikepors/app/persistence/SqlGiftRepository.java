package com.mikepors.app.persistence;

import com.mikepors.app.persistence.tools.SqlRunner;
import com.mikepors.gifttracker.core.CurrencyCode;
import com.mikepors.gifttracker.core.Money;
import com.mikepors.gifttracker.model.Gift;
import com.mikepors.gifttracker.model.Recipient;
import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyedEntity;
import com.mikepors.gifttracker.model.access.Repository;
import com.mikepors.gifttracker.model.access.impl.SimpleKey;
import com.mikepors.gifttracker.model.access.impl.SimpleKeyedEntity;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class SqlGiftRepository implements Repository<Gift> {

	@Autowired
	private Repository<Recipient> _recipientRepository;
	@Autowired
	private DataSource _dataSource;

	@Override
	public Key<Gift> save(Gift gift) {
		return new SqlRunner(_dataSource)
				.sql("INSERT\n")
				.sql("INTO gift (name, num_value, currency_value, recipient_id, is_gifted)\n")
				.sql("VALUES (?, ?, ?, ?, ?)")
				.parameter(gift.getName())
				.parameter(gift.getValue().getValueInMillionths())
				.parameter(gift.getValue().getCurrencyCode().name())
				.parameter(gift.getRecipient().getKey())
				.parameter(gift.isGifted())
				.insertSingleRow();
	}

	@Override
	public Gift get(Key<Gift> key) {
		return new SqlRunner(_dataSource)
				.sql("SELECT gift_id, name, num_value, currency_value, recipient_id, is_gifted\n")
				.sql("FROM gift\n")
				.sql("WHERE gift_id = ?").parameter(key)
				.getSingleEntity(new GiftInflater(_recipientRepository)::inflateEntity)
				.getEntity();
	}

	@Override
	public List<KeyedEntity<Gift>> getAll() {
		return new SqlRunner(_dataSource)
				.sql("SELECT gift_id, name, num_value, currency_value, recipient_id, is_gifted\n")
				.sql("FROM gift\n")
				.get(new GiftInflater(_recipientRepository)::inflateEntity);
	}


	private static class GiftInflater {

		private final Repository<Recipient> _recipientRepository;

		private GiftInflater(Repository<Recipient> recipientRepository) {
			_recipientRepository = recipientRepository;
		}

		private KeyedEntity<Gift> inflateEntity(ResultSet resultSet) throws SQLException {
			long giftId = resultSet.getLong("gift_id");
			String giftName = resultSet.getString("name");
			long valueInMillionths = resultSet.getLong("num_value");
			String currencyString = resultSet.getString("currency_value");
			long recipientId = resultSet.getLong("recipient_id");
			boolean isGifted = resultSet.getBoolean("is_gifted");

			SimpleKey<Recipient> recipientKey = new SimpleKey<>(recipientId);
			Recipient recipient = _recipientRepository.get(recipientKey);
			Gift gift = new Gift(giftName,
								 Money.ofMillionths(valueInMillionths, CurrencyCode.valueOf(currencyString)),
								 new SimpleKeyedEntity<>(recipientKey, recipient),
								 isGifted);
			return new SimpleKeyedEntity<>(new SimpleKey<>(giftId), gift);
		}
	}
}
