package com.mikepors.app.controllers;

import com.mikepors.gifttracker.core.CurrencyCode;
import com.mikepors.gifttracker.core.Money;
import com.mikepors.gifttracker.model.Gift;
import com.mikepors.gifttracker.model.Recipient;
import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyFactory;
import com.mikepors.gifttracker.model.access.KeyedEntity;
import com.mikepors.gifttracker.model.access.Repository;
import com.mikepors.gifttracker.model.access.impl.SimpleKey;
import com.mikepors.gifttracker.model.access.impl.SimpleKeyFactory;
import com.mikepors.gifttracker.model.access.impl.SimpleKeyedEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/gift")
public class GiftController {
	private final GiftFactory _giftFactory = new GiftFactory();

	@Autowired
	private Repository<Recipient> _recipientController;
	@Autowired
	private Repository<Gift> _giftRepository;

	private KeyFactory<Gift> _giftKeyFactory = new SimpleKeyFactory<>();


	@GetMapping("/get/{id}")
	public Gift getGift(@PathVariable Long id) {
		Key<Gift> giftKey = _giftKeyFactory.create(id);
		return _giftRepository.get(giftKey);
	}

	@PostMapping("/create")
	public Key<Gift> createGift(@RequestBody Map<String, String> payload) {
		long recipientId = Long.valueOf(payload.get("recipientId"));
		boolean isGifted = Boolean.valueOf(payload.get("isGifted"));
		String name = payload.get("name");
		int value = Integer.valueOf(payload.get("value"));

		Recipient recipient = _recipientController.get(new SimpleKey<>(recipientId));
		Gift gift = _giftFactory.create(name, Money.of(value, CurrencyCode.CAD), new SimpleKeyedEntity<>(new SimpleKey<>(recipientId), recipient), isGifted);
		return _giftRepository.save(gift);
	}

	@GetMapping("/all")
	public List<Gift> getAllGifts() {
		return _giftRepository.getAll()
				.stream()
				.map(KeyedEntity::getEntity)
				.collect(Collectors.toList());
	}

	private final class GiftFactory {
		Gift create(String name, Money value, KeyedEntity<Recipient> recipient, boolean gifted) {
			return new Gift(name, value, recipient, gifted);
		}
	}
}
