package com.mikepors.app.controllers;

import com.mikepors.gifttracker.model.Recipient;
import com.mikepors.gifttracker.model.access.Key;
import com.mikepors.gifttracker.model.access.KeyFactory;
import com.mikepors.gifttracker.model.access.KeyedEntity;
import com.mikepors.gifttracker.model.access.Repository;
import com.mikepors.gifttracker.model.access.impl.SimpleKeyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class RecipientController {

	private final RecipientFactory _recipientFactory = new RecipientFactory();
	private final KeyFactory<Recipient> _keyFactory = new SimpleKeyFactory<>();

	@Autowired
	private Repository<Recipient> _recipientRepository;

	@GetMapping("/recipient/{id}")
	public Recipient getRecipient(@PathVariable long id) {
		return _recipientRepository.get(_keyFactory.create(id));
	}

	@GetMapping("/recipient/all")
	public List<KeyedEntity<Recipient>> getRecipients() {
		return _recipientRepository.getAll();
	}

	@PostMapping("/recipient/create/")
	public Key<Recipient> createRecipient(@RequestBody Map<String, String> payload) {
		String recipientName = payload.get("name");
		Recipient recipient = _recipientFactory.create(recipientName);
		return _recipientRepository.save(recipient);
	}

	private static class RecipientFactory {
		Recipient create(String name) {
			return new Recipient(name);
		}
	}
}
