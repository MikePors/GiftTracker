package com.mikepors.app;

import com.mikepors.app.persistence.SqlGiftRepository;
import com.mikepors.app.persistence.SqlRecipientRepository;
import com.mikepors.gifttracker.model.Gift;
import com.mikepors.gifttracker.model.Recipient;
import com.mikepors.gifttracker.model.access.Repository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public Repository<Recipient> getRecipientRepository() {
		return new SqlRecipientRepository();
	}

	@Bean
	public Repository<Gift> getGiftRepository() {
		return new SqlGiftRepository();
	}

	@Bean
	DataSource getDataSource() {
		SQLiteDataSource dataSource = new SQLiteDataSource();
		dataSource.setUrl("jdbc:sqlite:database/myDb.sqlite");
		dataSource.setEnforceForeignKeys(true);


		return dataSource;
	}

}
