var AbstractService = function(requestFactory, parser) {
    this.requestFactory =  requestFactory;
    this.parser = parser;
};

AbstractService.prototype.get = function(onSuccess, onFailure, url) {
    var request = this.requestFactory.create();
    var parser = this.parser;
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var parsedResponse = parser.destringify(this.response);
            onSuccess(parsedResponse);
        } else if (this.readyState == 4) {
            onFailure(this.status);
        }
    };
    request.open("GET", url, true);
    request.setRequestHeader("Content-type", "application/json");
    request.send();
};


AbstractService.prototype.post = function (toPost, url) {
    var request = this.requestFactory.create();
    request.open('POST', url);
    request.setRequestHeader('Content-type', 'application/json');
    var stringifiedValue = this.parser.stringify(toPost);
    request.send(stringifiedValue);
};

if (typeof exports !== "undefined") {
    exports.AbstractService = AbstractService;
}