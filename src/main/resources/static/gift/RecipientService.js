var RecipientService = function(abstractService) {
    this.abstractService = abstractService;
}

RecipientService.prototype.getAll = function(onSuccess, onFailure) {
    return this.abstractService.get(onSuccess, onFailure, "/recipient/all");
}

RecipientService.prototype.save = function(recipient) {
    return this.abstractService.post(recipient, "/recipient/create/");
}

if (typeof exports !== "undefined") {
    exports.RecipientService = RecipientService;
}