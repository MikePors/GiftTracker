var GiftForm = function (giftNameFieldName, giftValueFieldName, giftRecipientFieldName, giftIsGiftedFieldName) {
    this.giftNameFieldName = giftNameFieldName;
    this.giftValueFieldName = giftValueFieldName;
    this.giftRecipientFieldName = giftRecipientFieldName;
    this.giftIsGiftedFieldName = giftIsGiftedFieldName;
};
GiftForm.prototype.get = function () {
    var nameField = document.getElementById(this.giftNameFieldName);
    var valueField = document.getElementById(this.giftValueFieldName);
    var recipientField = document.getElementById(this.giftRecipientFieldName);
    var isGiftedField = document.getElementById(this.giftIsGiftedFieldName);

    var gift = {};
    gift[nameField.name] = nameField.value;
    gift[valueField.name] = valueField.value;
    gift[recipientField.name] = recipientField.value;
    gift[isGiftedField.name] = isGiftedField.checked;
    return gift;
};