var RecipientSelector = function(id) {
    this.id = id;
};

RecipientSelector.prototype.updateOptions = function(keyedRecipients) {
    var recipientSelect = document.getElementById(this.id);
    removeAllChildren(recipientSelect);
    for (i = 0; i < keyedRecipients.length; i+=1) {
        var newOption = document.createElement("option");
        var keyedRecipient = keyedRecipients[i];
        var key = keyedRecipient.key;
        var recipient = keyedRecipient.entity;
        newOption.value = key.id;
        newOption.innerHTML = recipient.name;
        recipientSelect.appendChild(newOption);
    }
};

var removeAllChildren = function(node) {
  var children = node.children;
  for (i = children.length - 1; i >= 0; i-=1) {
    var child = children[i];
    node.removeChild(child);
  }
};