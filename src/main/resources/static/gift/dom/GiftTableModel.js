var GiftTableModel = function (domTable) {
    this.domTable = domTable;
};

GiftTableModel.prototype.clear = function () {
    var numberOfRows = this.domTable.rows.length;
    for (i = 0; i < numberOfRows - 1; i += 1) {
        table.deleteRow(-1);
    }
};

GiftTableModel.prototype.createRow = function (gift) {
    var newRow = this.domTable.insertRow(-1);

    var cellIndex = 0;

    var giftValue = gift['value'];
    newRow.insertCell(cellIndex++).innerHTML = gift.name;
    newRow.insertCell(cellIndex++).innerHTML = (giftValue.valueInMillionths / 1000000) + " " + giftValue.currencyCode;
    newRow.insertCell(cellIndex++).innerHTML = gift.recipient.entity.name;
    newRow.insertCell(cellIndex++).innerHTML = gift.gifted;
};

GiftTableModel.prototype.displayError = function (statusCode) {
    console.log(statusCode);
};
