var RecipientForm = function(recipientNameFieldId) {
    this.recipientNameFieldId = recipientNameFieldId;
}

RecipientForm.prototype.get = function() {
    var recipientNameField = document.getElementById(this.recipientNameFieldId);

    return {
        name: recipientNameField.value
    };
}