var GiftService = function (abstractService) {
    this.service = abstractService;
};

GiftService.prototype.getAll = function (onSuccess, onFailure) {
    this.service.get(onSuccess, onFailure, "/gift/all")
};


GiftService.prototype.save = function(gift) {
    this.service.post(gift, '/gift/create');
};

if (typeof exports !== 'undefined') {
    exports.GiftService = GiftService;
}