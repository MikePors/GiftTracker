var GiftController = function(giftTable, giftService, giftForm) {
    this.giftTable = giftTable;
    this.giftService = giftService;
    this.giftForm = giftForm;
};

GiftController.prototype.getAllGifts = function () {

    var giftTable = this.giftTable;
    var onSuccess = function(gifts) {
        giftTable.clear();
        for (i = 0; i< gifts.length; i++) {
            giftTable.createRow(gifts[i]);
        }
    };
    var onFailure = function (statusCode) {
        giftTable.displayError(statusCode);
    };
    this.giftService.getAll(onSuccess, onFailure);
};

GiftController.prototype.save = function() {
    var gift = this.giftForm.get();
    this.giftService.save(gift);
};

if (typeof exports !== "undefined") {
    exports.GiftController = GiftController;
}