var SimpleRequestFactory = function () {
    this.create = function () {
        return new XMLHttpRequest();
    }
};
var giftParser = {
    stringify: JSON.stringify,
    destringify: JSON.parse
};

var giftService = new GiftService(new AbstractService(new SimpleRequestFactory(), giftParser));

var giftTableModel = new GiftTableModel(document.getElementById("gift_table"));
var giftForm = new GiftForm('gift_name', 'gift_value', 'gift_recipient', 'gift_already_given');

var giftController = new GiftController(giftTableModel, giftService, giftForm);