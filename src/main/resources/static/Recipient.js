var keyedRecipientParser = {
    stringify: JSON.stringify,
    destringify: JSON.parse
}
var recipientService = new RecipientService(new AbstractService({
                                                                    create: function () {
                                                                        return new XMLHttpRequest();
                                                                    }
                                                                },
                                                                keyedRecipientParser));

var getRecipients = function() {
    var recipientSelector = new RecipientSelector("gift_recipient");
    var onSuccess = keyedRecipients => recipientSelector.updateOptions(keyedRecipients);
    var onFailure = function() {};
    recipientService.getAll(onSuccess, onFailure);;
};

var saveRecipient = function() {
  var recipient = new RecipientForm("recipient_name").get();
  recipientService.save(recipient);
};
