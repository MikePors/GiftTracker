CREATE TABLE recipient (recipient_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);

CREATE TABLE gift (gift_id INTEGER PRIMARY KEY AUTOINCREMENT,
		   recipient_id INTEGER,
                   name TEXT,
                   num_value INTEGER,
                   currency_value TEXT,
                   is_gifted INTEGER,
                   CONSTRAINT fk_recipient
                   FOREIGN KEY (recipient_id)
                   REFERENCES recipient(recipient_id));
